<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'contplaza_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e<j,6dWZkph)%Bn.(DHep%~3D@wdb~3U;A:&?#v$SX8bak8OcB%El:-#5R3wGX+8');
define('SECURE_AUTH_KEY',  'DA]XNOixb<1JclY<#bAr@YVZ&E-hl4#9|5xw{+Xhd1c[uz!VxX.|`Yw]v8TIyynU');
define('LOGGED_IN_KEY',    'ZB>m[-,^3*ST=:-f^TqqpL+e>+qk-qwL@KZ~H-{0B8qHjmv%G%B)9<96A+@ {Pyc');
define('NONCE_KEY',        '57u9-.mH@F46f9K)Bz-E7wP*ad5J0xV,p o}{#3IWc/, Bg,c-l$b)=8Cs3[}!#j');
define('AUTH_SALT',        'OE_J(vkAJ/gcy]!0My.2US|N1>SAoq-mI4e&,Kb<@$%NU4j;GxEaF?K3W_DkTozX');
define('SECURE_AUTH_SALT', ';KhnEs8F<L^*o^;KAjn;FU$Hu4/ea=A.h~0}hSK%EOVIMYd<:`<?(;aj-sfz8gLX');
define('LOGGED_IN_SALT',   '$Nh5VD56U0f9V42R uNJ#M&TjaTFfF,AlX./_lb{teqK}IZis t6.;W/cD,0lS(=');
define('NONCE_SALT',       'V!BM,0EKT_FUm{[]dW=3_#p(y{)SztsHlHh]F!qff2p #8H!s.~h`rl@=-el|:^ ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
