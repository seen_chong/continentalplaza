<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/tooltipster/tooltipster.bundle.min.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/tooltipster/tooltipster.bundle.min.js"></script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'emagid' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div id="headerWrapper">
			<div id="siteLogo">
				<a href="/">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/plaza-logo.png">
				</a>
			</div>

<!-- 			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><//?php esc_html_e( 'Primary Menu', 'emagid' ); ?></button>
				<//?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav> -->
		</div>
	</header><!-- #masthead -->
