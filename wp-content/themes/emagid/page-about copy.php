<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>
<style type="text/css">
/* Resize image <map> : apply and change background <area> for webkit */
img {display:block;margin:0 auto;height:100%;width:100%;}
div > img {position:absolute;top:0}

html {
background: #f06;
background: linear-gradient(45deg, #159, turquoise);
background-size:100% 100%;
height: 100%;
width:100%;
text-align:center;
display:table;
}
body > div{
transform-origin:center 0 ;}
#s025:checked  ~ div {transform:scale(0.25);margin:0 -600px -600px;transition:1s}
#s050:checked  ~ div {transform:scale(0.5);margin:0 -400px  -400px;transition:1s}
#s075:checked  ~ div {transform:scale(0.75);margin:0 -200px -200px;transition:1s}
#s100:checked  ~ div {transform:scale(1);margin:0 auto;transition:1s}
#s125:checked  ~ div {transform:scale(1.25);margin: 0 auto 200px;transition:1s}
body {display:table-cell;vertical-align:middle;}
map {position:relative;display:block;height:100%;width:100%;top:0;left:0;line-height:100vh;}
area {display:inline-block;width:5em;position:relative;z-index:1;line-height:1em;
background:rgba(0,0,0,0.25);color:white;line-height:40px;border-radius:3em/2em;}
/*area:after {content:attr(title);}*/
area.tooltip:hover {z-index:2;background:rgba(37, 37, 36, 0.65);box-shadow:0 0 0 2000px rgba(37, 37, 36, 0.65);}

area#tooltip1 {position:absolute;left:521px;top:430px;width:24px;height:24px;
	background-image: url(/wp-content/uploads/2016/12/tooltip.png);
	background-size: cover;
}
div.tooly {
	position: absolute;left:521px;top:430px;width:300px;height:200px;background-color: white;z-index: 111111;
}
area#tooltip2 {position:absolute;left:973px;top:341px;width:24px;height:24px;
	background-image: url(/wp-content/uploads/2016/12/tooltip.png);
	background-size: cover;
}
area#tooltip3 {position:absolute;left:73px;top:489px;width:24px;height:24px;
	background-image: url(/wp-content/uploads/2016/12/tooltip.png);
	background-size: cover;
}
area#tooltip4 {position:absolute;left:430px;top:733px;width:24px;height:24px;
	background-image: url(/wp-content/uploads/2016/12/tooltip.png);
	background-size: cover;
}
span.tool {position:relative;z-index:10;top:-1.4em;float:right;}
span.tool , a {text-shadow: 0 0 5px white, 0 0 10px white, 0 0 15px gold ;font-weight:bold;font-size:2em;padding:0 0.5em;color:#333}

</style>


	<div id="frontPageWrapper" >
<!-- style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/front-bg-alt_mini.jpg)" -->
<div>
 <map name="search">   
      <area class="tooltip" id="tooltip1" shape="rect" coords="521,430,648,484" tabindex="0">
      	<div class="tooly">erewrfer
      	</div>
      <area class="tooltip" id="tooltip2" shape="rect" coords="973,341,1050,379"tabindex="0">

      <area class="tooltip" id="tooltip3" shape="rect" coords="73,489,180,525" tabindex="0">

      <area class="tooltip" id="tooltip4" shape="rect" coords="430,733,510,784" tabindex="0">

  </map>
  <img  alt="itinéraire"  src="<?php echo get_template_directory_uri(); ?>/assets/img/front-bg-alt_mini.jpg"  usemap="#search" />
  </div>
<?php
get_footer();
?>