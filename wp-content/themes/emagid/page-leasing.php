<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div id="frontPageWrapper" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/front-bg-alt_mini.jpg)">

		<div class="leasing-sidebar">
			<div class="leasing-menu">
				<ul>
					<li><a href="#agents">Agents Contact</a></li>
<!-- 					<li><a href="#floorplans">Floor Plans</a></li>
					<li><a href="#brochure">Leasing Brochure</a></li> -->
					<li><a href="/gallery">Gallery</a></li>
				</ul>
			</div>
		</div>

		<div class="leasing-main">
			<div class="agents-contact-section">
				<div class="agents-contact-header">
					<div class="agents-contact-header-left">
						<h3>Please <a href="tel:12015555555">contact us</a> for further information or to schedule a building tour.</h3>
						<p>Immediate availability ranging from &#177;1,000 SF to &#177;50,000</p>
					</div>
					<div class="agents-contact-header-right">
<!-- 						<a href="/contact"><button>Book a Tour</button></a>
						<p><span class="dim">or call us </span>(201) 555 5555</p> -->
					</div>
				</div>

				<div class="agents-contact-team">
					<div class="agents-contact-team-header">
						<p>Our team at...</p>
						<img class="team-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/newmark-logo.png">
					</div>

					<div class="agents-contact-team-deck">

						<?php
                                $args = array(
                          'post_type' => 'team_members',
                          'posts_per_page' => '4'
                          );
                                $products = new WP_Query( $args );
                                      if( $products->have_posts() ) {
                                while( $products->have_posts() ) {
                                $products->the_post();
                          ?>
						<div class="agents-contact-team-card">	
							<div class="team-image">
								<img src="<?php the_field('headshot'); ?>">
							</div>
							<div class="team-text">
								<h6><?php the_field('position'); ?></h6>
								<h4><?php the_field('name'); ?></h4>
								<a href=""><span><?php the_field('email'); ?></span></a>
								<p><?php the_field('phone_number'); ?></p>
							</div>

						</div>

						<?php
                                }
                                      }
                                else {
                                echo 'No Team Members Found';
                                }
                          ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
?>