<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header('home'); ?>

	<div id="frontPageWrapper" style="background-image:url(<?php the_field('background'); ?>)">

	</div>
<?php
get_footer();
