<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>
<style type="text/css">
/* Resize image <map> : apply and change background <area> for webkit */


body > div{
transform-origin:center 0 ;}
#s025:checked  ~ div {transform:scale(0.25);margin:0 -600px -600px;transition:1s}
#s050:checked  ~ div {transform:scale(0.5);margin:0 -400px  -400px;transition:1s}
#s075:checked  ~ div {transform:scale(0.75);margin:0 -200px -200px;transition:1s}
#s100:checked  ~ div {transform:scale(1);margin:0 auto;transition:1s}
#s125:checked  ~ div {transform:scale(1.25);margin: 0 auto 200px;transition:1s}
body {display:table-cell;vertical-align:middle;overflow-x:hidden;}
map {position:relative;display:block;height:100%;width:100%;top:0;left:0;line-height:100vh;}

/*area:after {content:attr(title);}*/


</style>


	<div id="frontPageWrapper" >
<div>
 <map name="search">   
	  <area class="tooltip" id="tooltip1" shape="rect"  tabindex="0">
	  <area class="tooltip" id="tooltip2" shape="rect" tabindex="0">

	  <area class="tooltip" id="tooltip3" shape="rect"  tabindex="0">

	  <area class="tooltip" id="tooltip4" shape="rect"  tabindex="0">

	  <area class="tooltip" id="tooltip5" shape="rect"  tabindex="0">

	  <area class="tooltip" id="tooltip6" shape="rect"  tabindex="0">

	  <area class="tooltip" id="tooltip7" shape="rect"  tabindex="0">

	  <area class="tooltip" id="tooltip8" shape="rect"  tabindex="0">

  </map>

	<section>
	  <div class="tooltip-show" id="tooltip-show-1" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>Continental Plaza</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_1'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>
	  <div class="tooltip-show" id="tooltip-show-2" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>NJ Rail</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_2'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>
	  <div class="tooltip-show" id="tooltip-show-3" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>NJ Rail Parking Lot</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_3'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>
	  <div class="tooltip-show" id="tooltip-show-4" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>2 Shopping Centers</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_4'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>

	  <div class="tooltip-show" id="tooltip-show-5" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>NYC Skyline</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_5'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>

	  <div class="tooltip-show" id="tooltip-show-6" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>George Washington Bridge</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_6'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>

	 <div class="tooltip-show" id="tooltip-show-7" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>Newark International Airport</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_7'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>

	  <div class="tooltip-show" id="tooltip-show-8" style="display:none;">
	  	<div class="tooltip-header">
	  		<h5>Farleigh Dickinson University</h5>
	  	</div>
	  	<div class="tool-tip-body">
		  	<p><?php the_field('tooltip_8'); ?></p>
			<a href="/contact/">Contact Us</a>
	  	</div>
	  </div>

  </section>
  <img class="map-image" alt="itinéraire"  src="<?php echo get_template_directory_uri(); ?>/assets/img/front-bg-alt_mini.jpg"  usemap="#search" />
  </div>

  <script type="text/javascript">
		$("area#tooltip1").hover(function(e){
    	$("div#tooltip-show-1").toggle();
	});
		$("div#tooltip-show-1").hover(function(e){
    	$(this).toggle();
	});

		$("area#tooltip2").hover(function(e){
    	$("div#tooltip-show-2").toggle();
	});
		$("div#tooltip-show-2").hover(function(e){
    	$(this).toggle();
	});

		$("area#tooltip3").hover(function(e){
    	$("div#tooltip-show-3").toggle();
	});
		$("div#tooltip-show-3").hover(function(e){
    	$(this).toggle();
	});

		$("area#tooltip4").hover(function(e){
    	$("div#tooltip-show-4").toggle();
	});
		$("div#tooltip-show-4").hover(function(e){
    	$(this).toggle();
	});
		$("area#tooltip5").hover(function(e){
    	$("div#tooltip-show-5").toggle();
	});
		$("div#tooltip-show-5").hover(function(e){
    	$(this).toggle();
	});
	$("area#tooltip6").hover(function(e){
    	$("div#tooltip-show-6").toggle();
	});
		$("div#tooltip-show-6").hover(function(e){
    	$(this).toggle();
	});
	$("area#tooltip7").hover(function(e){
    	$("div#tooltip-show-7").toggle();
	});
		$("div#tooltip-show-7").hover(function(e){
    	$(this).toggle();
	});
	$("area#tooltip8").hover(function(e){
    	$("div#tooltip-show-8").toggle();
	});
		$("div#tooltip-show-8").hover(function(e){
    	$(this).toggle();
	});
  </script>
<?php
get_footer();
?>