<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
	<footer class="site-footer" role="contentinfo">
		<div class="footer-wrapper">
			<div id="footer-left">
				<p>&copy; 2016 All Rights Reserved</p>
			</div>
			<div id="footer-right">
				<p>concierge@continentalplaza.com</p>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
