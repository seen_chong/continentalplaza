<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div id="frontPageWrapper" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/front-bg-dark.jpg)" style="height:auto;">

		<div class="gallery-container">
			<div class="gallery-wrapper">

				<div class="mason-gallery">
				<div class="gallery-header">
					<h3>Aerial <span class="dim"> click on photo to zoom in</span></h3>
				</div>
					<?php echo do_shortcode('[gallery]'); ?>
				<div class="gallery-header">
					<h3>Exterior <span class="dim"> click on photo to zoom in</span></h3>
				</div>				
					<?php echo do_shortcode('[gallery]'); ?>
				<div class="gallery-header">
					<h3>Interior <span class="dim"> click on photo to zoom in</span></h3>
				</div>				
					<?php echo do_shortcode('[gallery]'); ?>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
?>