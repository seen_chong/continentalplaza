<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div id="frontPageWrapper" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/front-bg_mini.jpg)">

		<div class="contact-form-wrapper">
			<div class="contact-form-container">
				<div class="contact-form-header">
					<div class="header-left">
						<span>Mail</span>
						<p>401 Hackensack Ave,<br>Hackensack, NJ 07601</p>
					</div>
					<div class="header-right">
						<span>Phone</span>
						<p><span>TF</span>(201) 457 1037<br><span>FAX</span>(201) 457 0830</p>
					</div>
				</div>
				<div class="contact-form-body">
					<?php echo do_shortcode('[contact-form-7 id="31" title="Contact Form"]'); ?>
					<div class="cf-tooltip">

						<div id="cf-tool-general">
							<div class="cf-tooltip-top">
								<p>
								For General Inquiries, please contact:</p>
								<h5>Capstone Realty Group</h5>
							</div>
							<h6>(201) 555-5555</h6>
							<h6>info@crgre.com</h6>
						</div>						

						<div id="cf-tool-manage" style="display:none">
							<div class="cf-tooltip-top">
								<p>
								For Management Inquiries, please contact:</p>
								<h5>Faina Miller</h5>
								<p>
								<span>NEWMARK GRUBB KNIGHT FRANK (NGKF)</span></p>
							</div>
							<h6>(201) 555-5555</h6>
							<h6>Faina.miller@ngkf.com</h6>
						</div>

						<div id="cf-tool-construct" style="display:none">
							<div class="cf-tooltip-top">
								<p>
								For Construction Inquiries, please contact:</p>
								<h5>James D'agostino</h5>
								<p>
								<span>NEWMARK GRUBB KNIGHT FRANK (NGKF)</span></p>
							</div>
							<h6>(201) 555-5555</h6>
							<h6>Jamesj@jd-companies.com</h6>
						</div>						
					</div>
				</div>
			</div>

		</div>
	</div>


<script type="text/javascript">
$(document).ready(function(){
    $('select.wpcf7-form-control').on('change', function() {
      if ( this.value == 'General Inquiries')
      {
        $("#cf-tool-general").show();
        $("#cf-tool-manage").hide();
        $("#cf-tool-construct").hide();
      }

      else if ( this.value == 'Management Inquiries')
      {
        $("#cf-tool-manage").show();
        $("#cf-tool-general").hide();
        $("#cf-tool-construct").hide();
      }

      else if ( this.value == 'Construction Inquiries')
      {
        $("#cf-tool-construct").show();
        $("#cf-tool-general").hide();
        $("#cf-tool-manage").hide();
      }

      else 
      {
        $("#cf-tool-general").show();
        $("#cf-tool-manage").hide();
        $("#cf-tool-construct").hide();
      }
    });
});

</script>
<?php
get_footer();
?>

